Plugin Mathalea pour l'application Sésathèque
=============================================

Installation
------------

Pour être inclus dans une sésathèque, elle doit préciser dans son `_private/config.js`

```
plugins: {
  external: {
    '@sesatheque-plugins/mathalea': 'git+ssh://git@git.sesamath.net/sesamath/sesatheque-plugin-mathalea.git#x.y.z'
  }
}
```

Sur cette sésathèque
* `install` ajoute ou met à jour le plugin dans `app/plugins/node_modules/@sesatheque-plugins/mathalea`
* `build` compile les js du plugin (son webpack.config.js utilise app/plugins/webpack.config.js qui récupère les props `{entries, plugins, rules}` retournées par chaque `{plugin}/webpack.config.js`

Le build copie aussi le contenu de assets/ dans sesatheque:build/plugins/mathalea/ (cf notre webpack.config.js), ils seront donc accessible en http sur le domaine de la sésathèque avec l'url /plugins/mathalea/fichier.ext

Dépendances
-----------

La configuration webpack de la Sésathèque nous permet de faire des require de ses fichiers (relativement à son dossier app/ ou à ses node_modules). 

On utilise : 
* sesatheque
  * server/config
  * client/page/index
  * client-react
* modules externes (fournies par sesatheque)
  * sesajstools
  * prop-types
  * react
  * redux-form

Dev local
---------

`build:watch` fonctionne avec les js du plugin, mais pour éditer les fichiers du dépôt local du plugin et les voir recompiler à la volée, il faut
* dans le dépôt local du plugin faire un `yarn link`
* dans le dépôt local de la sésathèque faire un  `yarn link @sesatheque-plugins/mathalea`
* retrouver dans un dossier parent du dépôt local du plugin un dossier node_modules contenant les dépendances listées ci-dessus

Ça fonctionne avec par ex la structure
* workingDir
* workingDir/sesatheque (dépôt git)
* workingDir/sesatheque-plugin-mathalea (dépôt git)
* workingDir/node_modules -> sesatheque/node_modules
