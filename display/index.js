// on est compilé par le webpack de la sesatheque qui sait où c'est
import page from 'client/page'

const loaderUrl = 'https://coopmaths.fr/js/loader.js'

/**
 * Affiche une ressource mathalea
 * @service plugins/mathgraph/display
 * @param {Ressource}      ressource  L'objet ressource (une ressource mathgraph a en parametres soit une propriété url
 *                                      avec l'url du xml soit une propriété xml avec la string xml)
 * @param {displayOptions} options    Les options d'affichage
 * @param {HTMLElement} options.container  L'élément dans lequel afficher l'exercice mathalea
 * @param {function} options.resultatCallback  Une callback pour envoyer le résultat
 * @param {errorCallback}  next       La fct à appeler quand mathalea sera chargé (sans argument ou avec une erreur)
 */
export function display (ressource, { container, resultatCallback }, next) {
  let isNextCalled = false
  // si y'a pas de cb on affiche les erreurs sur la page (et en console)
  if (!next) {
    next = (error) => {
      if (error) page.addError(error)
    }
  }

  // une fct pour n'appeler next qu'une seule fois
  const callNextOnce = (error, app) => {
    if (isNextCalled) return log.error(error || Error('2e appel de next sans argument'))
    isNextCalled = true
    next(error)
  }

  try {
    if (!container) throw new Error('Il faut passer dans les options un conteneur html pour afficher cette ressource')

    // les params minimaux
    if (!ressource.oid || !ressource.titre || !ressource.parametres) throw new Error('Ressource incomplète')

    page.loadAsync([mtgLoadUrl], function () {
      try {
        if (typeof mathaleaLoader !== 'function') throw new Error('Mathalea n’est pas correctement chargé')
        // Ajouter l'activité dans container et envoyer le résultat via resultatCallback

      } catch (error) {
        callNextOnce(error)
      }
    })
  } catch (error) {
    callNextOnce(error)
  }
}
