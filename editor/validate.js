/**
 * Valide une ressource éditée
 * @param {Ressource} ressource La ressource à éditer
 * @param {Object} ressource.parametres Les paramètres de la ressource
 * @param {Object} errors
 */
export default function validate ({ parametres }, errors) {
  console.info('validate mathalea avec', parametres, errors)
  // pour ajouter une erreur, c'est
  // errors.paramX = 'Le paramètre paramX a tel pb'
}
