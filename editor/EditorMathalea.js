import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { formValues } from 'redux-form'

class EditorMathalea extends Component {
  render () {
    return (
      <div>
        Mettre ici l'éditeur d'exercice Mathaléa
      </div>
    )
  }
}

EditorMathalea.propTypes = {
  parametres: PropTypes.object.isRequired
}

export default formValues({ parametres: 'parametres' })(EditorMathalea)
