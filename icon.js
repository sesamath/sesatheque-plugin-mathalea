import icon from './assets/mathalea.gif'
import type from './type'

export { type, icon }
